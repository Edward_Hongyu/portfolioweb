(function($) {
  "use strict";

  $.fn.andSelf = function() {
    return this.addBack.apply(this, arguments);
  }

  $(window).on("load", function() {
    $(".section-loader").fadeOut("slow");

  });


  $('.navbar-nav>li>a').on('click', function() {
    $('.navbar-collapse').collapse('hide');
  });
  // nav
  $('#zb-header').onePageNav({
    currentClass: 'active',
    changeHash: false,
    scrollSpeed: 750,
    scrollThreshold: 0.5,
  });

  $(window).on('scroll', function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
      $(".nav-scroll").addClass("nav-strict");
    } else {
      $(".nav-scroll").removeClass("nav-strict");
    }
  });

  $(window).on('scroll', function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
      $(".nav-scroll").addClass("nav-strict");
    } else {
      $(".nav-scroll").removeClass("nav-strict");
    }
  });
  // owl owlCarousel
  $('#advice_section_std').owlCarousel({
    loop: true,
    responsiveClass: true,
    nav: true,
    navText: ["<i class='fas fa-long-arrow-alt-left'></i>", "<i class='fas fa-long-arrow-alt-right'></i>"],
    autoplay: true,
    smartSpeed: 450,
    margin: 30,
    autoWidth: true,
    stopOnHover: true,
    autoplay: false,
    // stagePadding: 190,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 5,
      },
      768: {
        items: 2,
      },
      1170: {
        items: 2,
      }
    }

  });


  $('#advice_section').owlCarousel({
    loop: true,
    responsiveClass: true,
    nav: false,
    autoplay: true,
    smartSpeed: 2500,
    margin: 20,
    autoWidth: true,
    stopOnHover: true,
    autoplay: true,
    autoplayTimeout: 100,
    stagePadding: 100,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1,
      },
      768: {
        items: 2,
      },
      1170: {
        items: 3,
      }
    }

  });

  $(".advice_slider .owl-next").on("click", function() {
    $(".advice_slider .owl-prev").addClass("op_1");
  });


  function mediaSize() {
    if (window.matchMedia('(min-width: 768px)').matches) {
      //parallax
      var image = document.getElementsByClassName('ione');
      new simpleParallax(image, {
        overflow: true,
        scale: 1.3,
        breakpoints: 420,
      });

      var image = document.getElementsByClassName('itwo');
      new simpleParallax(image, {
        overflow: true,
        scale: 1.2,
        orientation: 'right',
        breakpoints: 420,
      });

      var image = document.getElementsByClassName('ithree');
      new simpleParallax(image, {
        overflow: true,
        scale: 1.4,
        breakpoints: 420,
      });
      var image4 = document.getElementsByClassName('ifour');
      new simpleParallax(image4, {
        overflow: true,
        scale: 5.5,
        orientation: 'left',
        delay: .7,
        transition: 'cubic-bezier(0,0,0,100)',
        breakpoints: 420,

      });

    } else {

      var image = document.getElementsByClassName('ione');
      new simpleParallax(image, {
        overflow: true,
        scale: 1.0,
        breakpoints: 420,
      });

      var image = document.getElementsByClassName('itwo');
      new simpleParallax(image, {
        overflow: true,
        scale: 1.0,
        orientation: 'none',
        breakpoints: 420,
      });

      var image = document.getElementsByClassName('ithree');
      new simpleParallax(image, {
        overflow: true,
        scale: 1.0,
        breakpoints: 420,
      });
      var image4 = document.getElementsByClassName('ifour');
      new simpleParallax(image4, {
        overflow: true,
        scale: 1.0,
        orientation: 'left',
        breakpoints: 420,

      });

    }
  };

  mediaSize();
}(jQuery));
// for google_map
window.$ = $;
window.jQuery = $;
let script = document.createElement('script');
script.src = 'http://maps.googleapis.com/maps/api/js?key=AIzaSyCT0qXoUVRMxjjaVjhXjxO_fM4TvjeQbPs&callback=initMap';
script.defer = true;
script.async = true;
window.initMap = function() {
  let myhome = {
    lat: -37.879260,
    lng: 145.048520
  };
  let map;
  map = new google.maps.Map(document.getElementById('map'), {
    center: {
      lat: -37.879260,
      lng: 145.048520
    },
    zoom: 17,
    styles: [{
        elementType: 'geometry',
        stylers: [{
          color: '#242f3e'
        }]
      },
      {
        elementType: 'labels.text.stroke',
        stylers: [{
          color: '#242f3e'
        }]
      },
      {
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#746855'
        }]
      },
      {
        featureType: 'administrative.locality',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#d59563'
        }]
      },
      {
        featureType: 'poi',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#d59563'
        }]
      },
      {
        featureType: 'poi.park',
        elementType: 'geometry',
        stylers: [{
          color: '#263c3f'
        }]
      },
      {
        featureType: 'poi.park',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#6b9a76'
        }]
      },
      {
        featureType: 'road',
        elementType: 'geometry',
        stylers: [{
          color: '#38414e'
        }]
      },
      {
        featureType: 'road',
        elementType: 'geometry.stroke',
        stylers: [{
          color: '#212a37'
        }]
      },
      {
        featureType: 'road',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#9ca5b3'
        }]
      },
      {
        featureType: 'road.highway',
        elementType: 'geometry',
        stylers: [{
          color: '#746855'
        }]
      },
      {
        featureType: 'road.highway',
        elementType: 'geometry.stroke',
        stylers: [{
          color: '#1f2835'
        }]
      },
      {
        featureType: 'road.highway',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#f3d19c'
        }]
      },
      {
        featureType: 'transit',
        elementType: 'geometry',
        stylers: [{
          color: '#2f3948'
        }]
      },
      {
        featureType: 'transit.station',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#d59563'
        }]
      },
      {
        featureType: 'water',
        elementType: 'geometry',
        stylers: [{
          color: '#17263c'
        }]
      },
      {
        featureType: 'water',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#515c6d'
        }]
      },
      {
        featureType: 'water',
        elementType: 'labels.text.stroke',
        stylers: [{
          color: '#17263c'
        }]
      }
    ]
  });
  var marker = new google.maps.Marker({
    position: myhome,
    map: map,
    title:'Come and fine me in here!'
  });
}
document.head.appendChild(script);
